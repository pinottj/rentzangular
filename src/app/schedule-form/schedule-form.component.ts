import { Component, OnInit, Output, ViewEncapsulation } from '@angular/core';
import {Schedule} from '../models/schedule';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-schedule-form',
  templateUrl: './schedule-form.component.html',
  styleUrls: ['./schedule-form.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ScheduleFormComponent implements OnInit {
  
  @Output() schedule = new Schedule();
  submitted = false;
  failed = false;
  patterns = {
    email: /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/
  };

  constructor(private http: HttpClient) {}
  
  ngOnInit() {
  }

  onSubmit() {
    // reset failed message
    this.failed = false;
    // generate json body from schedule form
    var body = {
      email: this.schedule.email,
      date: this.schedule.date,
      time: this.schedule.time
    };
    var nowDate = new Date();
    var auth = 'scorpioSoftware'+nowDate.toISOString().slice(0,10).replace(/-/g,"")+'valicsi!';
    // console.log(btoa(auth));
    // Send data, HTTP request
    
    // this.http.post('http://localhost:3000/schedule', body, 
    this.http.post('http://api.scorpiosoftwarellc.com/schedule', body, 
    { 
      headers: new HttpHeaders().set('Authorization', btoa(auth))
              .set('Content-Type', 'application/json')
              .set('Access-Control-Allow-Origin','*')
              .set('Accept', 'application/json')
    }
    ).subscribe(
      // Successful responses call the first callback.
      data => {
        console.log("Email: " + this.schedule.email + ", Date: " + this.schedule.date + ", Time: " + this.schedule.time);
        this.submitted = true;
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          console.log('An error occurred:', err.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.log(`Backend returned code ${err.status}, error body is: ` + JSON.stringify(err.error));
          this.failed = true;
        }
      }
    );
  }

  // get diagnostic() { return JSON.stringify(this.schedule); }
}
