import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HousesComponent } from './houses/houses.component';
import { HouseDetailComponent } from './houses/house-detail/house-detail.component';
import { HouseItemComponent } from './houses/house-item/house-item.component';
import { FooterComponent } from './footer/footer.component';
import { ScheduleFormComponent } from './schedule-form/schedule-form.component';
// Import HttpClientModule from @angular/common/http
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HousesComponent,
    HouseDetailComponent,
    HouseItemComponent,
    FooterComponent,
    ScheduleFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
