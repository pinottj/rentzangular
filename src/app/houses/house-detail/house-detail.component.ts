import { Component, OnInit, Output, ViewEncapsulation } from '@angular/core';
// import { FormControl, FormGroup } from '@angular/forms';
import {House} from '../../models/house';
@Component({
  selector: 'app-house-detail',
  templateUrl: './house-detail.component.html',
  styleUrls: ['./house-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HouseDetailComponent implements OnInit {
  @Output() house: House;

  constructor() { 
    // this.house = new House(
    //   '', '', 'Columbus', 'GA', 31907, 680, 'Single Family', 
    //   'Ft. Benning, High School, Shopping', 3, 1, 'Range/Oven', 
    //   'Internet', '1,000', 'Carpet', 'Central Cooling', 
    //   'Forced Air, Gas', .25, ['blueridge.jpg', 'property-img6.jpg'], true);
    this.house = new House();
    this.house.number = '';
    this.house.street = '';
    this.house.city = 'Columbus';
    this.house.state = 'GA';
    this.house.zip = 31907;
    this.house.rent= 670;
    this.house.type = 'Single Family';
    this.house.nearBy = 'Ft. Benning, High School, Shopping';
    this.house.bedrooms = 3;
    this.house.baths = 1;
    this.house.appliances = 'Range/Oven';
    this.house.amenities = 'Internet';
    this.house.sqft = '1,000';
    this.house.floorType = 'Ceramic Tile';
    this.house.coolingType = 'Central Cooling';
    this.house.heatingType = 'Forced Air, Gas';
    this.house.acres = .25;
    this.house.imagePath = ['property-img1.jpg', 'property-img2.jpg', 'property-img3.jpg', 'property-img4.jpg', 'property-img5.jpg', 'property-img6.jpg'];
    this.house.isActive = true;
  }

  ngOnInit() {
  }
}
