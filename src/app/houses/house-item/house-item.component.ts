import { Component, OnInit, Output, ViewEncapsulation } from '@angular/core';
import {House} from '../../models/house';
@Component({
  selector: 'app-house-item',
  templateUrl: './house-item.component.html',
  styleUrls: ['./house-item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HouseItemComponent implements OnInit {
  @Output() house: House;
  
  constructor() { 
    this.house.number = ''
    this.house.street = ''
    this.house.city = '';
    this.house.state = 'GA';
    this.house.zip = 31907;
  }

  ngOnInit() {
  }

}
