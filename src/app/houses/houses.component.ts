import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HousesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
