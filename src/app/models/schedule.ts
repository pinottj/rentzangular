import { Time } from "@angular/common/src/i18n/locale_data_api";

export class Schedule {
    email: string;
    date: Date;
    time: Time;

    constructor();
    constructor(email?: string,
        date?: Date,
        time?: Time){};
}