export class House {
    number: string;
    street: string;
    city: string;
    state: string;
    zip: number;
    rent: number;
    type: string;
    nearBy: string; 
    bedrooms: number;
    baths: number;
    appliances: string;
    amenities: string;
    sqft: string;
    floorType: string;
    coolingType: string;
    heatingType: string;
    acres: number;
    imagePath: string[];
    isActive: boolean;

    constructor();
    constructor(number?: string,
        street?: string,
        city?: string,
        state?: string,
        zip?: number,
        rent?: number,
        type?: string,
        nearBy?: string, 
        bedrooms?: number,
        baths?: number,
        appliances?: string,
        amenities?: string,
        sqft?: string,
        floorType?: string,
        coolingType?: string,
        heatingType?: string,
        acres?: number,
        imagePath?:string[],
        isActive?: boolean){};
}